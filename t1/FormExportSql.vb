﻿Public Class FormExportSql

    Private refInstance As FormPrincipale
    Private tabIndexControle As Integer = 0
    Private connexion As Sql

    Public Sub New(refInst As FormPrincipale)
        InitializeComponent()

        refInstance = refInst
    End Sub

    ''' <summary>
    ''' Créer un DGV avec une ligne pour les correspondances MySQL et des lignes d'exemples
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub FormExportSql_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim colIndex As Integer
        Dim ligne As DataGridViewRow
        Dim ligneCorresp As DataGridViewRow = New DataGridViewRow()
        ligneCorresp.HeaderCell.Value = "Propriétés table correspondantes"
        ligneCorresp.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        ligneCorresp.ReadOnly = False

        dgv_liste.Rows.Add(ligneCorresp)
        For i As Integer = 0 To 3
            colIndex = 0
            ligne = refInstance.dgv_liste.Rows(i).Clone()
            ligne.DefaultCellStyle.BackColor = Color.LightGray
            ligne.ReadOnly = True

            For Each c As DataGridViewCell In refInstance.dgv_liste.Rows(i).Cells
                ligne.Cells(colIndex).Value = c.Value
                colIndex += 1
            Next

            dgv_liste.Rows.Add(ligne)
        Next

        dgv_liste.RowHeadersWidth = ligneCorresp.HeaderCell.PreferredSize.Width


    End Sub

    ''' <summary>
    ''' Lance l'exporation du jeu d'essais avec une base de données
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btn_exportMySQL_Click(sender As Object, e As EventArgs) Handles btn_exportMySQL.Click
        If connexion Is Nothing Then
            btn_testConnexion.PerformClick()
        ElseIf Not connexion Is Nothing AndAlso rtb_script.Text.Trim().Length > 0 Then
            Try
                connexion.execReq(rtb_script.Text)
            Catch ex As Exception
                MessageBox.Show("Erreur : " + vbCr + ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        End If
    End Sub

    ''' <summary>
    ''' Génère le script pour l'exportation vers une base de données
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btn_genScript_Click(sender As Object, e As EventArgs) Handles btn_genScript.Click
        'INSERT INTO table (propTable) VALUES (résultats)

        Dim table As String = txt_nomTable.Text.Trim()

        If table.Length > 0 Then
            Dim contenuTable As String = ""

            Dim prop As String = ""

            Dim propTable As String = ""
            Dim listePropTable As List(Of String) = New List(Of String)

            Dim lignesCount As Integer = refInstance.dgv_liste.RowCount

            Dim colTable As List(Of String) = New List(Of String)
            Dim colTableCount As Integer = 0

            Dim entree As String = ""
            Dim lignesEntrees As String = ""

            Dim succes As Boolean = True

            rtb_script.Clear()

            'on ajoute les propriétés renseignées dans la chaine propTable
            For i As Integer = 0 To dgv_liste.ColumnCount - 1

                If dgv_liste.Rows(0).Cells(i).Value <> "" Then
                    colTable.Add(dgv_liste.Columns(i).Name)

                    If i <> 0 Then
                        propTable += ", "
                    End If

                    prop = dgv_liste.Rows(0).Cells(i).Value.ToString()

                    'on regarde si la propriété n'a pas déjà été ajouté
                    If Not listePropTable.Contains(prop) Then
                        listePropTable.Add(prop)
                        propTable += prop
                    Else
                        succes = False
                        Exit For
                    End If
                End If

            Next

            If succes Then

                colTableCount = colTable.Count
                If colTableCount <> 0 Then

                    ' on génère ou non le script pour créer la table
                    If ckb_genTables.Checked Then
                        For i As Integer = 0 To colTableCount - 1
                            If i <> 0 Then
                                contenuTable += ", " + vbCr
                            End If

                            contenuTable += String.Format("`{0}` VARCHAR(50) DEFAULT NULL", dgv_liste.Rows(0).Cells(colTable.Item(i)).Value.ToString())
                        Next

                        rtb_script.Text += String.Format("CREATE TABLE IF NOT EXISTS `{0}` ({1}{2}{1});", table, vbCr, contenuTable)

                        rtb_script.Text += vbCr + vbCr
                    End If

                    'traitement des valeurs à ajouter
                    For i As Integer = 0 To lignesCount - 1
                        If i <> 0 Then
                            lignesEntrees += ", " + vbCr
                        End If

                        For j As Integer = 0 To colTableCount - 1
                            If j <> 0 Then
                                entree += ", "
                            End If

                            entree += String.Format("'{0}'", refInstance.dgv_liste.Rows(i).Cells(colTable(j)).Value.ToString().Replace("'", "\'"))
                        Next

                        lignesEntrees += "(" + entree + ")"
                        entree = ""
                    Next

                    rtb_script.Text += String.Format("INSERT INTO `{0}` ({1}) VALUES {2}{3};", table, propTable, vbCr, lignesEntrees)

                Else

                    MessageBox.Show("Aucune propriétés de table spécifiées.", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

                End If
            Else

                MessageBox.Show("Une des propriétés figure plusieurs fois.", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

            End If

        End If
    End Sub

    ''' <summary>
    ''' Permet d'effectuer un test de connexion et récupère l'objet si concluant
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btn_testConnexion_Click(sender As Object, e As EventArgs) Handles btn_testConnexion.Click
        Dim formConnexion As FormSql = New FormSql(connexion)
        If formConnexion.ShowDialog() = DialogResult.OK Then
            connexion = formConnexion.Connexion
            lb_infosConnexion.Text = String.Format("Serveur : {0} - BdD : {1} - Identifiant : {2}", connexion.Serveur, connexion.BdD, connexion.Identifiant)

            btn_exportMySQL.Enabled = True
        End If
    End Sub

    ''' <summary>
    ''' Indique la volonté ou non de créer le script pour générer la table associée avec le jeu
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ckb_genTables_CheckedChanged(sender As Object, e As EventArgs) Handles ckb_genTables.CheckedChanged
        If rtb_script.Text.Length > 0 Then
            btn_genScript.PerformClick()
        End If
    End Sub
End Class