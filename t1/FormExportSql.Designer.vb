﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormExportSql
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.rtb_script = New System.Windows.Forms.RichTextBox()
        Me.btn_exportMySQL = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dgv_liste = New System.Windows.Forms.DataGridView()
        Me.nom_dgc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.prenom_dgc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.adresse_dgc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ville_dgc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tel_dgc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.courriel_dgc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btn_genScript = New System.Windows.Forms.Button()
        Me.txt_nomTable = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lb_infosConnexion = New System.Windows.Forms.Label()
        Me.btn_testConnexion = New System.Windows.Forms.Button()
        Me.ckb_genTables = New System.Windows.Forms.CheckBox()
        CType(Me.dgv_liste, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'rtb_script
        '
        Me.rtb_script.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rtb_script.Location = New System.Drawing.Point(12, 197)
        Me.rtb_script.Name = "rtb_script"
        Me.rtb_script.ReadOnly = True
        Me.rtb_script.Size = New System.Drawing.Size(779, 186)
        Me.rtb_script.TabIndex = 9
        Me.rtb_script.Text = ""
        '
        'btn_exportMySQL
        '
        Me.btn_exportMySQL.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_exportMySQL.Location = New System.Drawing.Point(716, 389)
        Me.btn_exportMySQL.Name = "btn_exportMySQL"
        Me.btn_exportMySQL.Size = New System.Drawing.Size(75, 23)
        Me.btn_exportMySQL.TabIndex = 10
        Me.btn_exportMySQL.Text = "Exporter"
        Me.btn_exportMySQL.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(385, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Indiquez les correspondances entre les colonnes et les propriétés de votre table." & _
    ""
        '
        'dgv_liste
        '
        Me.dgv_liste.AllowUserToAddRows = False
        Me.dgv_liste.AllowUserToDeleteRows = False
        Me.dgv_liste.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgv_liste.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgv_liste.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllHeaders
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv_liste.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgv_liste.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv_liste.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.nom_dgc, Me.prenom_dgc, Me.adresse_dgc, Me.ville_dgc, Me.tel_dgc, Me.courriel_dgc})
        Me.dgv_liste.Location = New System.Drawing.Point(12, 25)
        Me.dgv_liste.Name = "dgv_liste"
        Me.dgv_liste.Size = New System.Drawing.Size(779, 140)
        Me.dgv_liste.TabIndex = 1
        '
        'nom_dgc
        '
        Me.nom_dgc.HeaderText = "Nom"
        Me.nom_dgc.Name = "nom_dgc"
        '
        'prenom_dgc
        '
        Me.prenom_dgc.HeaderText = "Prénom"
        Me.prenom_dgc.Name = "prenom_dgc"
        '
        'adresse_dgc
        '
        Me.adresse_dgc.HeaderText = "Adresse"
        Me.adresse_dgc.Name = "adresse_dgc"
        '
        'ville_dgc
        '
        Me.ville_dgc.HeaderText = "Ville"
        Me.ville_dgc.Name = "ville_dgc"
        '
        'tel_dgc
        '
        Me.tel_dgc.HeaderText = "Téléphone"
        Me.tel_dgc.Name = "tel_dgc"
        '
        'courriel_dgc
        '
        Me.courriel_dgc.HeaderText = "Courriel"
        Me.courriel_dgc.Name = "courriel_dgc"
        '
        'btn_genScript
        '
        Me.btn_genScript.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_genScript.Location = New System.Drawing.Point(489, 389)
        Me.btn_genScript.Name = "btn_genScript"
        Me.btn_genScript.Size = New System.Drawing.Size(105, 23)
        Me.btn_genScript.TabIndex = 8
        Me.btn_genScript.Text = "Générer le script"
        Me.btn_genScript.UseVisualStyleBackColor = True
        '
        'txt_nomTable
        '
        Me.txt_nomTable.Location = New System.Drawing.Point(147, 171)
        Me.txt_nomTable.Name = "txt_nomTable"
        Me.txt_nomTable.Size = New System.Drawing.Size(142, 20)
        Me.txt_nomTable.TabIndex = 7
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 174)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(129, 13)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Nom de la table à remplir :"
        '
        'lb_infosConnexion
        '
        Me.lb_infosConnexion.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lb_infosConnexion.AutoSize = True
        Me.lb_infosConnexion.Location = New System.Drawing.Point(12, 394)
        Me.lb_infosConnexion.Name = "lb_infosConnexion"
        Me.lb_infosConnexion.Size = New System.Drawing.Size(0, 13)
        Me.lb_infosConnexion.TabIndex = 11
        '
        'btn_testConnexion
        '
        Me.btn_testConnexion.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_testConnexion.Location = New System.Drawing.Point(600, 389)
        Me.btn_testConnexion.Name = "btn_testConnexion"
        Me.btn_testConnexion.Size = New System.Drawing.Size(110, 23)
        Me.btn_testConnexion.TabIndex = 12
        Me.btn_testConnexion.Text = "Test de connexion"
        Me.btn_testConnexion.UseVisualStyleBackColor = True
        '
        'ckb_genTables
        '
        Me.ckb_genTables.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ckb_genTables.AutoSize = True
        Me.ckb_genTables.Location = New System.Drawing.Point(638, 174)
        Me.ckb_genTables.Name = "ckb_genTables"
        Me.ckb_genTables.Size = New System.Drawing.Size(153, 17)
        Me.ckb_genTables.TabIndex = 13
        Me.ckb_genTables.Text = "Générer également la table"
        Me.ckb_genTables.UseVisualStyleBackColor = True
        '
        'FormExportSql
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(803, 424)
        Me.Controls.Add(Me.ckb_genTables)
        Me.Controls.Add(Me.btn_testConnexion)
        Me.Controls.Add(Me.lb_infosConnexion)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txt_nomTable)
        Me.Controls.Add(Me.btn_genScript)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dgv_liste)
        Me.Controls.Add(Me.btn_exportMySQL)
        Me.Controls.Add(Me.rtb_script)
        Me.Name = "FormExportSql"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.Text = "Exportation MySQL"
        CType(Me.dgv_liste, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents rtb_script As System.Windows.Forms.RichTextBox
    Friend WithEvents btn_exportMySQL As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dgv_liste As System.Windows.Forms.DataGridView
    Friend WithEvents btn_genScript As System.Windows.Forms.Button
    Friend WithEvents txt_nomTable As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents nom_dgc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents prenom_dgc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents adresse_dgc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ville_dgc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tel_dgc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents courriel_dgc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lb_infosConnexion As System.Windows.Forms.Label
    Friend WithEvents btn_testConnexion As System.Windows.Forms.Button
    Friend WithEvents ckb_genTables As System.Windows.Forms.CheckBox
End Class
