﻿Public Class Options

    Private _nomAttributCSV As String
    Private _cheminCSV As String
    Private _indexCol As Integer
    Private _separateur As Char
    Private _commencerPremLigne As Boolean

#Region "Accesseurs"

    Public Property NomAttributCSV() As String
        Get
            Return _nomAttributCSV
        End Get
        Set(ByVal value As String)
            _nomAttributCSV = value
        End Set
    End Property

    Public Property CheminCSV() As String
        Get
            Return _cheminCSV
        End Get
        Set(ByVal value As String)
            _cheminCSV = value
        End Set
    End Property

    Public Property IndexCol() As Integer
        Get
            Return _indexCol
        End Get
        Set(ByVal value As Integer)
            _indexCol = value
        End Set
    End Property

    Public Property Separateur() As Char
        Get
            Return _separateur
        End Get
        Set(ByVal value As Char)
            _separateur = value
        End Set
    End Property

    Public Property CommencerPremLigne() As String
        Get
            Return _commencerPremLigne
        End Get
        Set(ByVal value As String)
            _commencerPremLigne = value
        End Set
    End Property

#End Region

    Public Sub New()
        _nomAttributCSV = ""
        _cheminCSV = ""
        _indexCol = -1
        _separateur = ""
        _commencerPremLigne = True
    End Sub

    Public Sub New(nomAttribut As String, chemin As String, col As Integer, sep As Char, premLign As Boolean)
        _nomAttributCSV = nomAttribut
        _cheminCSV = chemin
        _indexCol = col
        _separateur = sep
        _commencerPremLigne = premLign
    End Sub

End Class
