﻿Imports System.ComponentModel

Public Class FormSql

    Private _connexion As Sql
    Private connexionOk As Boolean = False
    Private WithEvents workerConnexion As BackgroundWorker = New BackgroundWorker()

    Public ReadOnly Property Connexion() As Sql
        Get
            Return _connexion
        End Get
    End Property

    Public Sub New(c As Sql)
        InitializeComponent()

        If Not c Is Nothing Then
            txt_serveur.Text = c.Serveur
            txt_bdd.Text = c.BdD
            txt_identifiant.Text = c.Identifiant
        End If
    End Sub

    Private Sub FormSql_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        workerConnexion.WorkerSupportsCancellation = True

        'gestion du lancement du worker
        AddHandler workerConnexion.DoWork, AddressOf tentativeConnexion
        AddHandler workerConnexion.RunWorkerCompleted, AddressOf finConnexion
    End Sub

    ''' <summary>
    ''' Test de la connexion
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btn_connexion_Click(sender As Object, e As EventArgs) Handles btn_connexion.Click
        btn_connexion.Enabled = False
        progressBar.Style = ProgressBarStyle.Marquee
        progressBar.Enabled = True

        workerConnexion.RunWorkerAsync()
    End Sub

    ''' <summary>
    ''' Connexion asynchrone
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub tentativeConnexion()
        Try
            _connexion = New Sql(txt_serveur.Text.Trim(), txt_bdd.Text.Trim(), txt_identifiant.Text.Trim(), txt_mdp.Text.Trim())

            connexionOk = _connexion.testConnexion()
        Catch ex As Exception
            MessageBox.Show(
                "Erreur de connexion : " + vbCr +
                ex.Message,
                "",
                MessageBoxButtons.OK,
                MessageBoxIcon.Error
                )
        End Try
    End Sub

    ''' <summary>
    ''' Retour à l'état initial de la fenêtre
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub finConnexion()
        If connexionOk Then
            Me.DialogResult = Windows.Forms.DialogResult.OK
            Close()
        Else
            btn_connexion.Enabled = True
            progressBar.Enabled = False
            progressBar.Style = ProgressBarStyle.Blocks
        End If
    End Sub

    ''' <summary>
    ''' Autorise l'essai uniquement si l'IP/l'URL, la base de données et le nom utilisateur sont renseignés. Mot de passe facultatif
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub textBox_TextChanged(sender As Object, e As EventArgs) Handles txt_serveur.TextChanged, txt_identifiant.TextChanged, txt_bdd.TextChanged
        If txt_bdd.Text.Trim().Length > 0 AndAlso txt_serveur.Text.Trim().Length > 0 AndAlso txt_identifiant.Text.Trim().Length > 0 Then
            btn_connexion.Enabled = True
        Else
            btn_connexion.Enabled = False
        End If
    End Sub
End Class