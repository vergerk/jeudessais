﻿Imports System.IO
Imports System.Xml.Serialization
Imports System.Text
Imports System.Text.RegularExpressions

Public Class FormPrincipale

    Private liste_noms As List(Of String) = New List(Of String)
    Private liste_prenoms As List(Of String) = New List(Of String)
    Private liste_adresses As List(Of String) = New List(Of String)
    Private liste_villes As List(Of String) = New List(Of String)

    Private liste_jeuDEssai As List(Of JeuDEssai) = New List(Of JeuDEssai)
    Private liste_Options As List(Of Options) = New List(Of Options)

    Private estCharge As Boolean = False

    Private pattern As Regex = New Regex("\s*\(\w*\)", RegexOptions.None)

#Region "Formulaire"

    ''' <summary>
    ''' A la fin du chargement du formulaire
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub FormPrincipale_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        liste_noms.AddRange(New String() {"Martin", "Bernard", "Thomas", "Petit", "Robert", "Richard", "Durand", "Dubois", "Moreau", "Laurent", "Simon", "Michel", "Lefebvre", "Leroy", "Roux", "David", "Bertrand", "Morel", "Fournier", "Girard", "Bonnet", "Dupont", "Lambert", "Fontaine", "Rousseau", "Vincent", "Muller", "Lefèvre", "Faure", "André", "Mercier", "Blanc", "Guérin", "Boyer", "Garnier", "Chevalier", "François", "Legrand", "Gauthier", "Garcia", "Perrin", "Robin", "Clément", "Morin", "Nicolas", "Henry", "Roussel", "Mathieu", "Gautier", "Masson", "Marchand", "Duval", "Denis", "Dumont", "Marie", "Lemaire", "Noël", "Meyer", "Dufour", "Meunier", "Brun", "Blanchard", "Giraud", "Joly", "Rivière", "Lucas", "Brunet", "Gaillard", "Barbier", "Arnaud", "Martinez", "Gérard", "Roche", "Renard", "Schmitt", "Roy", "Leroux", "Colin", "Vidal", "Caron", "Picard", "Roger", "Fabre", "Aubert", "Lemoine", "Renaud", "Dumas", "Lacroix", "Olivier", "Philippe", "Bourgeois", "Pierre", "Benoît", "Rey", "Leclerc", "Payet", "Rolland", "Leclercq", "Guillaume", "Lecomte", "Lopez", "Jean", "Dupuy", "Guillot", "Hubert", "Berger", "Carpentier", "Sanchez", "Dupuis", "Moulin", "Louis", "Deschamps", "Huet", "Vasseur", "Perez", "Boucher", "Fleury", "Royer", "Klein", "Jacquet", "Adam", "Paris", "Poirier", "Marty", "Aubry", "Guyot", "Carré", "Charles", "Renault", "Charpentier", "Ménard", "Maillard", "Baron", "Bertin", "Bailly", "Hervé", "Schneider", "Fernandez", "Le Gall", "Collet", "Léger", "Bouvier", "Julien", "Prévost", "Millet", "Perrot", "Daniel", "Le Roux", "Cousin", "Germain", "Breton", "Besson", "Langlois", "Rémy", "Le Goff", "Pelletier", "Lévêque", "Perrier", "Leblanc", "Barré", "Lebrun", "Marchal", "Weber", "Mallet", "Hamon", "Boulanger", "Jacob", "Monnier", "Michaud", "Rodriguez", "Guichard", "Gillet", "Étienne", "Grondin", "Poulain", "Tessier", "Chevallier", "Collin", "Chauvin", "Da Silva", "Bouchet", "Gay", "Lemaître", "Bénard", "Maréchal", "Humbert", "Reynaud", "Antoine", "Hoarau", "Perret", "Barthélemy", "Cordier", "Pichon", "Lejeune", "Gilbert", "Lamy", "Delaunay", "Pasquier", "Carlier", "Laporte"})

        cb_listeExports.Items.AddRange(New String() {"XML / CSV", "MySQL"})
        cb_listeExports.SelectedIndex = 0

        liste_Options = Chargement(GetType(List(Of Options)), "options")

        For Each opt As Options In liste_Options
            EditeFormulaireOptions(opt)
        Next

        ControleListe()

        estCharge = True
    End Sub

    ''' <summary>
    ''' Quand le formulaire se ferme
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub FormPrincipale_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed

        Sauvegarde(GetType(List(Of Options)), "options.xml", liste_Options)

    End Sub

#End Region

#Region "Sauvegarde / chargement"

    ''' <summary>
    ''' Sauvegarde d'une classe au format XML
    ''' </summary>
    ''' <param name="nomDeClasse"></param>
    ''' <param name="fichier"></param>
    ''' <param name="obj"></param>
    ''' <remarks></remarks>
    Private Sub Sauvegarde(nomDeClasse As Type, fichier As String, obj As Object)

        Try
            Dim xs As XmlSerializer = New XmlSerializer(nomDeClasse)
            Dim sw As StreamWriter = New StreamWriter(fichier, False, Encoding.UTF8)
            xs.Serialize(sw, obj)
        Catch ex As Exception
            MessageBox.Show(
                "Il y a eu un problème lors de la sauvegarde.\n " + ex.Message,
                "",
                MessageBoxButtons.OK,
                MessageBoxIcon.Error
                )
        End Try

    End Sub

    ''' <summary>
    ''' Chargement d'un fichier XML
    ''' </summary>
    ''' <param name="nomDeClasse"></param>
    ''' <param name="fichier"></param>
    ''' <remarks></remarks>
    Private Function Chargement(nomDeClasse As Type, fichier As String)

        Dim o As Object = Nothing

        ' on regarde si le fichier existe
        If (File.Exists(fichier + ".xml")) Then

            Try
                Dim xs As XmlSerializer = New XmlSerializer(nomDeClasse)
                Dim sr As StreamReader = New StreamReader(fichier + ".xml", Encoding.UTF8)
                o = xs.Deserialize(sr)
                sr.Close()
            Catch ex As Exception
                MessageBox.Show(
                    "Il y a eu un problème lors de la sauvegarde.\n " + ex.Message,
                    "",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error
                    )
            End Try

        End If

        Return o

    End Function

    ''' <summary>
    ''' Exportation XML ou CSV
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub Btn_export_Click(sender As Object, e As EventArgs) Handles btn_export.Click
        Select Case cb_listeExports.SelectedItem
            Case "XML / CSV"
                exportFichier()
            Case "MySQL"
                Dim mysql As FormExportSql = New FormExportSql(Me)
                mysql.ShowDialog(Me)
        End Select
    End Sub

    ''' <summary>
    ''' Export dans un fichier
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub exportFichier()
        If SaveFileDialog.ShowDialog() = DialogResult.OK Then

            Dim filter As Integer = SaveFileDialog.FilterIndex
            Dim fichier As String = SaveFileDialog.FileName

            ' switch
            Select Case filter
                Case 1 'XML
                    Sauvegarde(GetType(List(Of JeuDEssai)), fichier, liste_jeuDEssai)

                Case 2 'CSV
                    Dim sw As StreamWriter = New StreamWriter(fichier, False, Encoding.UTF8)
                    Dim ligne As String = ""
                    Dim i As Integer

                    ' on ajoute l'intitulé des colonnes
                    For Each col As DataGridViewColumn In dgv_liste.Columns
                        ligne += col.HeaderText
                        If i < dgv_liste.Columns.Count - 1 Then
                            ligne += ";"
                        End If
                    Next
                    sw.WriteLine(ligne)

                    ' on parcourt toute la ligne pour l'inscrire dans le CSV
                    For Each row As DataGridViewRow In dgv_liste.Rows
                        i = 0
                        ligne = ""
                        For Each c As DataGridViewCell In row.Cells
                            ligne += c.Value.ToString()

                            If i < row.Cells.Count - 1 Then
                                ligne += ";"
                            End If
                            i += 1
                        Next
                        sw.WriteLine(ligne)
                    Next

                    sw.Close()
            End Select

        End If
    End Sub

#End Region

#Region "Contrôle"

    ''' <summary>
    ''' Contrôle le contenu d'une chaine en vue d'une conversion en entier. Retourne 0 si la chaine est vide sinon la chaine d'entrée
    ''' </summary>
    ''' <param name="v"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ControleTextBox(v As String)
        Dim r = 0

        If v <> "" Then
            r = v
        End If

        Return r
    End Function

#End Region

#Region "Génération"

    ''' <summary>
    ''' Génère le DataGridView avec le jeu de données
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub Btn_genDgv_Click(sender As Object, e As EventArgs) Handles Btn_genDgv.Click

        Dim champsRemplis As Boolean = True

        ' On parcourt chaque contrôle et on vérifie que les TextBox sont bien remplis.
        For i As Integer = 0 To Me.Controls.Count - 1

            If TypeName(Me.Controls.Item(i)) = "TextBox" Then

                ' Si l'un d'entre eux est est vide en étant activé, le bouton de génération reste grisé
                If (String.IsNullOrWhiteSpace(Me.Controls.Item(i).Text) Or Me.Controls.Item(i).Text = "") AndAlso Me.Controls.Item(i).Enabled Then
                    champsRemplis = False
                End If
            End If

        Next

        ' On regarde si l'extraction a bien été faite
        If liste_adresses.Count = 0 OrElse liste_noms.Count = 0 OrElse liste_prenoms.Count = 0 OrElse liste_villes.Count = 0 Then
            champsRemplis = False
        End If

        If champsRemplis Then

            Btn_genDgv.Enabled = False

            ' sauvegarde de la sélection de l'utilisateur
            AjoutOuModifie(New Options("Prenoms", txt_prenoms.Text, Convert.ToInt16(ControleTextBox(txt_colPrenoms.Text)), txt_sepPrenoms.Text, ckb_prenoms.Enabled))
            AjoutOuModifie(New Options("Noms", txt_noms.Text, Convert.ToInt16(ControleTextBox(txt_colNoms.Text)), txt_sepNoms.Text, ckb_noms.Enabled))
            AjoutOuModifie(New Options("Adresses", txt_adresses.Text, Convert.ToInt16(ControleTextBox(txt_colAdresses.Text)), txt_sepAdresses.Text, ckb_adresses.Enabled))
            AjoutOuModifie(New Options("Villes", txt_villes.Text, Convert.ToInt16(ControleTextBox(txt_colVilles.Text)), txt_sepVilles.Text, ckb_villes.Enabled))

            Dim rand As New Random(Integer.Parse(Guid.NewGuid().ToString().Substring(0, 8), System.Globalization.NumberStyles.HexNumber))

            Dim nom As String
            Dim prenom As String
            Dim coordonnees As String
            Dim ville As String
            Dim telephone As String
            Dim courriel As String

            Dim max As Int16 = 100

            ProgressBar.Maximum = max
            btn_export.Enabled = False

            For i = 0 To max

                If liste_prenoms.Count > 0 Then

                    nom = liste_noms(rand.Next(0, liste_noms.Count - 1))
                    prenom = liste_prenoms(rand.Next(0, liste_prenoms.Count - 1))
                    prenom = prenom.ToUpper().Substring(0, 1) + prenom.Substring(1, prenom.Length - 1)
                    coordonnees = liste_adresses(rand.Next(0, liste_adresses.Count - 1))
                    ville = liste_villes(rand.Next(0, liste_villes.Count - 1))
                    telephone = GenTelephone()
                    courriel = GenCourriel(nom, prenom, ville.Substring(0, 5))

                    dgv_liste.Rows.Add(
                        nom,
                        prenom,
                        coordonnees,
                        ville,
                        telephone,
                        courriel
                        )

                    'création d'une liste
                    liste_jeuDEssai.Add(New JeuDEssai(nom, prenom, coordonnees, ville, telephone, courriel))

                    ProgressBar.Value = i

                End If

            Next

            Btn_genDgv.Enabled = True
            btn_export.Enabled = True
            ProgressBar.Value = 0

        Else
            MessageBox.Show(
                "Certaines données sont manquantes. Après avoir sélectionné les fichiers, avez-vous appuyer sur " + vbFormFeed + "Extr." + vbFormFeed + "pour lancer l'extraction ?",
                "",
                MessageBoxButtons.OK,
                MessageBoxIcon.Error
                )
        End If

    End Sub

    ''' <summary>
    ''' Gestion des téléphones - groupe ## ## ## ## ##
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GenTelephone(Optional indicatifZero As Boolean = True)

        Dim rand As New Random(Integer.Parse(Guid.NewGuid().ToString().Substring(0, 8), System.Globalization.NumberStyles.HexNumber))
        Dim tel As String = rand.Next(100000000, 999999999).ToString()
        Dim indicatif As String

        If indicatifZero Then
            indicatif = "0"
        Else
            indicatif = tel.Substring(0, 1)
        End If

        Return String.Format("0{0} {1} {2} {3} {4}", indicatif, tel.Substring(1, 2), tel.Substring(3, 2), tel.Substring(5, 2), tel.Substring(7, 2))

    End Function

    ''' <summary>
    ''' Génère des adresses e-mails (d'après RFC 3696)
    ''' </summary>
    ''' <param name="nom"></param>
    ''' <param name="prenom"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GenCourriel(nom As String, prenom As String, cp As String)

        ' https://fr.wikipedia.org/wiki/Adresse_%C3%A9lectronique#Syntaxe_exacte

        Dim rand As New Random(Integer.Parse(Guid.NewGuid().ToString().Substring(0, 8), System.Globalization.NumberStyles.HexNumber))
        Dim separateur As String = "._-!#$%&' *+/=?\^`{|}~"
        Dim serveur As String() = {"omh", "epic", "turkoiz", "anonymel", "karamel", "alphazed", "vb"}
        Dim extension As String() = {"fr", "org", "com", "ch", "xyz", "net"}
        Dim sep As String = rand.Next(0, separateur.Count - 1).ToString()

        Dim retour As String = ""

        Dim probFormatEmail As Integer = rand.Next(1, 100)
        Dim probPrenom As Integer = rand.Next(1, 100)
        Dim probNom As Integer = rand.Next(1, 100)
        Dim probSep As Integer = rand.Next(1, 100)
        Dim probCP As Integer = rand.Next(1, 100)
        Dim probServeur As Integer = rand.Next(1, 100)

        ' format possible d'une adresse email
        ' [nom/prenom][separateur][prenom/nom/cp/valeurNum] @ [NdD serveur / IP]
        ' on considère que nom.prenom@fai.fr ou prenom.nom@fai.fr constitue la majorité des adressses emails

        ' PRENOM ''''''''''''''''''''''''''
        If probPrenom < 30 Then
            prenom = prenom.Substring(0, 1)
        End If

        ' NOM '''''''''''''''''''''''''''''
        If probNom < 10 Then
            nom = nom.Substring(0, 1)
        End If

        ' SEPARATEUR ''''''''''''''''''''''
        If probSep < 5 Then ' prob de 5% d'avoir un caractère spécial hors ceux communs
            sep = (separateur.Substring(3, separateur.Count - 3))(rand.Next(0, separateur.Count - 3))
        Else ' prob de 95% d'avoir . _ ou -
            sep = (separateur.Substring(0, 3))(rand.Next(0, 3))
        End If

        ' FORMAT EMAIL ''''''''''''''''''''
        ' on choisi le format final d'après le modulo du nombre, par 2
        If probFormatEmail Mod 2 = 0 Then
            retour += prenom + sep + nom
        Else
            retour += nom + sep + prenom
        End If

        ' CODE POSTAL ''''''''''''''''''''
        If probFormatEmail > 80 Then ' 20% des cas restants, avec un nombre à la fin (code postal par exemple)

            If probCP > 70 Then ' dans 30% des cas, ce sera un nombre aléatoire
                retour += rand.Next(1, 99).ToString()
            Else ' sinon le code postal
                If probCP Mod 2 = 0 Then
                    retour += cp
                Else
                    retour += cp.Substring(0, 2)
                End If
            End If

        End If

        retour += "@"

        ' SERVEUR ''''''''''''''''''''''''
        If probServeur < 95 Then
            retour += serveur(rand.Next(0, serveur.Count - 1)) + "." + extension(rand.Next(0, extension.Count - 1))

        Else ' le serveur peut être une adresse IP
            retour += String.Format("{0}.{1}.{2}.{3}", rand.Next(1, 254).ToString(), rand.Next(1, 254).ToString(), rand.Next(1, 254).ToString(), rand.Next(1, 254).ToString())

        End If

        Return retour.ToLower().Replace(" ", "")

    End Function

    ''' <summary>
    ''' Extraction des données du CSV + insertion dans la liste
    ''' </summary>
    ''' <param name="fichier"></param>
    ''' <param name="separateur"></param>
    ''' <param name="colonne"></param>
    ''' <param name="checkbox"></param>
    ''' <param name="liste"></param>
    ''' <remarks></remarks>
    Private Sub Extraction(fichier As String, separateur As String, colonne As String, checkbox As Boolean, liste As List(Of String))
        If File.Exists(fichier) Then
            Dim part As String()
            Dim resultat As String()
            Dim sep As Char = separateur
            Dim col As Int16 = Convert.ToInt16(colonne)
            Dim i As Integer = 0

            ' on évite la ligne des en-têtes si la case est décochée
            If Not checkbox Then
                i = 1
            End If

            For Each l As String In File.ReadLines(fichier)
                part = l.Split(sep)
                If i <> 0 AndAlso Not String.IsNullOrEmpty(part(col)) Then
                    resultat = pattern.Split(part(col))
                    liste.Add(resultat(0))
                End If

                i += 1
            Next
        Else
            MessageBox.Show("Le fichier au chemin suivant est introuvable : " + vbCr + fichier)
        End If
    End Sub

    ''' <summary>
    ''' Gestion des extractions des données
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btn_extraction_Click(sender As Object, e As EventArgs) Handles btn_extrPrenoms.Click, btn_extrNoms.Click, btn_extrAdresses.Click, btn_extrVilles.Click

        Select Case CType(sender, Button).Name

            Case "btn_extrPrenoms"
                If Not String.IsNullOrWhiteSpace(txt_sepPrenoms.Text) AndAlso Not String.IsNullOrWhiteSpace(txt_colPrenoms.Text) AndAlso Not String.IsNullOrWhiteSpace(txt_sepPrenoms.Text) AndAlso btn_prenoms.Enabled Then
                    Extraction(txt_prenoms.Text, txt_sepPrenoms.Text, txt_colPrenoms.Text, ckb_prenoms.Enabled, liste_prenoms)
                    lb_cptePrenoms.Text = liste_prenoms.Count
                Else
                    Erreur_MsgBox()
                End If
            Case "btn_extrNoms"
                If Not String.IsNullOrWhiteSpace(txt_sepNoms.Text) AndAlso Not String.IsNullOrWhiteSpace(txt_colNoms.Text) AndAlso Not String.IsNullOrWhiteSpace(txt_sepNoms.Text) AndAlso btn_noms.Enabled Then
                    Extraction(txt_noms.Text, txt_sepNoms.Text, txt_colNoms.Text, ckb_noms.Enabled, liste_noms)
                    lb_cpteNoms.Text = liste_noms.Count
                Else
                    Erreur_MsgBox()
                End If
            Case "btn_extrAdresses"
                If Not String.IsNullOrWhiteSpace(txt_sepAdresses.Text) AndAlso Not String.IsNullOrWhiteSpace(txt_colAdresses.Text) AndAlso Not String.IsNullOrWhiteSpace(txt_sepAdresses.Text) AndAlso btn_adresses.Enabled Then
                    Extraction(txt_adresses.Text, txt_sepAdresses.Text, txt_colAdresses.Text, ckb_adresses.Enabled, liste_adresses)
                    lb_cpteAdresses.Text = liste_adresses.Count
                Else
                    Erreur_MsgBox()
                End If
            Case "btn_extrVilles"
                If Not String.IsNullOrWhiteSpace(txt_sepVilles.Text) AndAlso Not String.IsNullOrWhiteSpace(txt_colVilles.Text) AndAlso Not String.IsNullOrWhiteSpace(txt_sepVilles.Text) AndAlso btn_villes.Enabled Then
                    'Extraction(txt_villes.Text, txt_sepVilles.Text, txt_colVilles.Text, ckb_villes.Enabled, liste_villes)

                    Dim part As String()
                    Dim sep As Char = txt_sepVilles.Text()
                    Dim i As Integer = 0
                    Dim code As String
                    Dim ville As String

                    ' on évite la ligne des en-têtes si la case est décochée
                    If Not ckb_villes.Enabled Then
                        i = 1
                    End If

                    For Each l As String In File.ReadLines(txt_villes.Text)
                        If i <> 0 Then
                            part = l.Split(sep)

                            code = part(2)

                            'cas où le code postal ne fait pas 5 de longueur
                            Do While code.Length < 5
                                code = "0" + code
                            Loop

                            If String.IsNullOrEmpty(part(4)) Then
                                ville = part(3)
                            Else
                                ville = part(4)
                            End If

                            liste_villes.Add(String.Format("{0} {1}", code, ville))
                        End If
                        i += 1
                    Next

                    lb_cpteVilles.Text = liste_villes.Count

                Else
                    Erreur_MsgBox()
                End If

        End Select

    End Sub

#End Region

#Region "Divers"

    ''' <summary>
    ''' Boite de dialogue pour les fichiers
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub selectionFichier(sender As Object, e As EventArgs) Handles btn_prenoms.Click, btn_villes.Click, btn_noms.Click, btn_adresses.Click

        If OpenFileDialog.ShowDialog() = DialogResult.OK Then
            Select Case CType(sender, Button).Name
                Case "btn_prenoms"
                    txt_prenoms.Text = OpenFileDialog.FileName
                Case "btn_noms"
                    txt_noms.Text = OpenFileDialog.FileName
                Case "btn_adresses"
                    txt_adresses.Text = OpenFileDialog.FileName
                Case "btn_villes"
                    txt_villes.Text = OpenFileDialog.FileName
            End Select
        End If

    End Sub

    ''' <summary>
    ''' Méthode pour comparer l'option sauvegardée avec celle présente dans la liste. Si absente, l'ajoute à cette liste
    ''' </summary>
    ''' <param name="opt"></param>
    ''' <remarks></remarks>
    Private Sub AjoutOuModifie(opt As Options)

        Dim ajouter As Boolean = True

        For Each o As Options In liste_Options

            If o.NomAttributCSV = opt.NomAttributCSV Then
                ajouter = False

                ' rien à ajouter, on met à jour les options
                o.CheminCSV = opt.CheminCSV
                o.CommencerPremLigne = opt.CommencerPremLigne
                o.IndexCol = opt.IndexCol
                o.Separateur = opt.Separateur

                o = opt
                Exit For 'équivalent du break;
            End If

        Next

        If ajouter Then
            liste_Options.Add(opt)
        End If

    End Sub

    ''' <summary>
    ''' Méthode pour charger l'option dans les champs du formulaire
    ''' </summary>
    ''' <param name="opt"></param>
    ''' <remarks></remarks>
    Private Sub EditeFormulaireOptions(opt As Options)

        Select Case opt.NomAttributCSV
            Case "Prenoms"
                txt_prenoms.Text = opt.CheminCSV
                txt_colPrenoms.Text = opt.IndexCol
                txt_sepPrenoms.Text = opt.Separateur
                ckb_prenoms.Enabled = opt.CommencerPremLigne
            Case "Noms"
                txt_noms.Text = opt.CheminCSV
                txt_colNoms.Text = opt.IndexCol
                txt_sepNoms.Text = opt.Separateur
                ckb_noms.Enabled = opt.CommencerPremLigne
            Case "Adresses"
                txt_adresses.Text = opt.CheminCSV
                txt_colAdresses.Text = opt.IndexCol
                txt_sepAdresses.Text = opt.Separateur
                ckb_adresses.Enabled = opt.CommencerPremLigne
            Case "Villes"
                txt_villes.Text = opt.CheminCSV
                txt_colVilles.Text = opt.IndexCol
                txt_sepVilles.Text = opt.Separateur
                ckb_villes.Enabled = opt.CommencerPremLigne
        End Select

    End Sub

    ''' <summary>
    ''' Erreurs lors du remplissage du DGV
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub Erreur_MsgBox()
        MessageBox.Show("Des données sont manquantes. Avez-vous rempli toute la ligne ?", "", MessageBoxButtons.OK, MessageBoxIcon.Error)
    End Sub

    ''' <summary>
    ''' Désactive les contrôles relatifs à une liste si cette dernière est remplie à la compilation
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ControleListe()
        If liste_adresses.Count > 0 Then
            btn_adresses.Enabled = False
            txt_adresses.Enabled = False
            txt_colAdresses.Enabled = False
            txt_sepAdresses.Enabled = False
            btn_extrAdresses.Enabled = False
            ckb_adresses.Enabled = False
        End If

        If liste_prenoms.Count > 0 Then
            btn_prenoms.Enabled = False
            txt_prenoms.Enabled = False
            txt_colPrenoms.Enabled = False
            txt_sepPrenoms.Enabled = False
            btn_extrPrenoms.Enabled = False
            ckb_prenoms.Enabled = False
        End If

        If liste_noms.Count > 0 Then
            btn_noms.Enabled = False
            txt_noms.Enabled = False
            txt_colNoms.Enabled = False
            txt_sepNoms.Enabled = False
            btn_extrNoms.Enabled = False
            ckb_noms.Enabled = False
        End If

        If liste_villes.Count > 0 Then
            btn_villes.Enabled = False
            txt_villes.Enabled = False
            txt_colVilles.Enabled = False
            txt_sepVilles.Enabled = False
            btn_extrVilles.Enabled = False
            ckb_villes.Enabled = False
        End If
    End Sub

#End Region

    ''' <summary>
    ''' Formulaire de crédits
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btn_aProposDe_Click(sender As Object, e As EventArgs) Handles btn_aProposDe.Click
        FormCredits.ShowDialog()
    End Sub
End Class
