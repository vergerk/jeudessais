﻿Imports MySql.Data.MySqlClient

Public Class Sql
    Private _serveur As String
    Private _bdd As String
    Private _identifiant As String
    Private mdp As String
    Private connexion As MySqlConnection

    Public Property Serveur As String
        Get
            Return _serveur
        End Get
        Set(ByVal value As String)
            _serveur = value
        End Set
    End Property

    Public Property BdD As String
        Get
            Return _bdd
        End Get
        Set(ByVal value As String)
            _bdd = value
        End Set
    End Property

    Public Property Identifiant As String
        Get
            Return _identifiant
        End Get
        Set(ByVal value As String)
            _identifiant = value
        End Set
    End Property

#Region "Constructeur"

    ''' <summary>
    ''' Création de l'objet de connexion
    ''' </summary>
    ''' <param name="serveur"></param>
    ''' <param name="BdD"></param>
    ''' <param name="utilisateur"></param>
    ''' <param name="mdp"></param>
    ''' <remarks></remarks>
    Sub New(serveur As String, BdD As String, utilisateur As String, mdp As String)
        Me._serveur = serveur
        Me._bdd = BdD
        Me._identifiant = utilisateur
        Me.mdp = mdp

        connexion = New MySqlConnection("Database=" + BdD + ";Data Source=" + serveur + ";User Id=" + utilisateur + ";Password=" + mdp)
    End Sub

#End Region

#Region "Méthodes"
    ''' <summary>
    ''' Requête sans retour de données
    ''' </summary>
    ''' <param name="requete"></param>
    ''' <remarks></remarks>
    Sub execReq(requete As String)
        Try
            If connexion.State = ConnectionState.Closed Then
                connexion.Open()
            End If

            Dim sql As MySqlDataAdapter = New MySqlDataAdapter()
            sql.SelectCommand = New MySqlCommand(requete, connexion)
            sql.SelectCommand.ExecuteNonQuery()
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally
            finConnexion()
        End Try
    End Sub

    ''' <summary>
    ''' Requête avec retour de données
    ''' </summary>
    ''' <param name="requete"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function execReq_retour(requete As String)
        Dim resultatsReq As DataSet = New DataSet()

        Try
            If connexion.State = ConnectionState.Closed Then
                connexion.Open()
            End If

            Dim reponse As MySqlDataAdapter = New MySqlDataAdapter(requete, connexion)
            reponse.Fill(resultatsReq)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally
            finConnexion()
        End Try

        Return resultatsReq
    End Function

    ''' <summary>
    ''' Fermeture de la connexion
    ''' </summary>
    ''' <remarks></remarks>
    Sub finConnexion()
        If connexion.State = ConnectionState.Open Then
            connexion.Close()
        End If
    End Sub

    ''' <summary>
    ''' Test de la connexion
    ''' </summary>
    ''' <remarks></remarks>
    Function testConnexion()
        Dim succes As Boolean = False

        Try
            connexion.Open()
            connexion.Close()

            succes = True
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

        Return succes
    End Function
#End Region

End Class