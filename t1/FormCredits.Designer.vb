﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormCredits
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.rtb_credits = New System.Windows.Forms.RichTextBox()
        Me.btn_fermer = New System.Windows.Forms.Button()
        Me.pb_icone = New System.Windows.Forms.PictureBox()
        Me.lb_application = New System.Windows.Forms.Label()
        CType(Me.pb_icone, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'rtb_credits
        '
        Me.rtb_credits.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rtb_credits.BackColor = System.Drawing.SystemColors.Control
        Me.rtb_credits.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.rtb_credits.Cursor = System.Windows.Forms.Cursors.Help
        Me.rtb_credits.Location = New System.Drawing.Point(12, 114)
        Me.rtb_credits.Name = "rtb_credits"
        Me.rtb_credits.ReadOnly = True
        Me.rtb_credits.Size = New System.Drawing.Size(340, 180)
        Me.rtb_credits.TabIndex = 0
        Me.rtb_credits.Text = ""
        '
        'btn_fermer
        '
        Me.btn_fermer.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_fermer.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_fermer.Location = New System.Drawing.Point(277, 300)
        Me.btn_fermer.Name = "btn_fermer"
        Me.btn_fermer.Size = New System.Drawing.Size(75, 23)
        Me.btn_fermer.TabIndex = 1
        Me.btn_fermer.Text = "Fermer"
        Me.btn_fermer.UseVisualStyleBackColor = True
        '
        'pb_icone
        '
        Me.pb_icone.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pb_icone.Location = New System.Drawing.Point(256, 12)
        Me.pb_icone.Name = "pb_icone"
        Me.pb_icone.Size = New System.Drawing.Size(96, 96)
        Me.pb_icone.TabIndex = 2
        Me.pb_icone.TabStop = False
        '
        'lb_application
        '
        Me.lb_application.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lb_application.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lb_application.Location = New System.Drawing.Point(12, 9)
        Me.lb_application.Name = "lb_application"
        Me.lb_application.Size = New System.Drawing.Size(238, 99)
        Me.lb_application.TabIndex = 3
        Me.lb_application.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'FormCredits
        '
        Me.AcceptButton = Me.btn_fermer
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.CancelButton = Me.btn_fermer
        Me.ClientSize = New System.Drawing.Size(364, 335)
        Me.Controls.Add(Me.lb_application)
        Me.Controls.Add(Me.pb_icone)
        Me.Controls.Add(Me.btn_fermer)
        Me.Controls.Add(Me.rtb_credits)
        Me.Cursor = System.Windows.Forms.Cursors.Help
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormCredits"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "A propos de cette application"
        CType(Me.pb_icone, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents rtb_credits As System.Windows.Forms.RichTextBox
    Friend WithEvents btn_fermer As System.Windows.Forms.Button
    Friend WithEvents pb_icone As System.Windows.Forms.PictureBox
    Friend WithEvents lb_application As System.Windows.Forms.Label
End Class
