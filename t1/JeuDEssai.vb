﻿Public Class JeuDEssai

    Private _nom As String
    Private _prenom As String
    Private _adresse As String
    Private _ville As String
    Private _telephone As String
    Private _courriel As String

#Region "Accesseurs"

    Public Property Nom() As String
        Get
            Return _nom
        End Get
        Set(ByVal value As String)
            _nom = value
        End Set
    End Property

    Public Property Prenom() As String
        Get
            Return _prenom
        End Get
        Set(ByVal value As String)
            _prenom = value
        End Set
    End Property

    Public Property Adresse() As String
        Get
            Return _adresse
        End Get
        Set(ByVal value As String)
            _adresse = value
        End Set
    End Property

    Public Property Ville() As String
        Get
            Return _ville
        End Get
        Set(ByVal value As String)
            _ville = value
        End Set
    End Property

    Public Property Telephone() As String
        Get
            Return _telephone
        End Get
        Set(ByVal value As String)
            _telephone = value
        End Set
    End Property

    Public Property Courriel() As String
        Get
            Return _courriel
        End Get
        Set(ByVal value As String)
            _courriel = value
        End Set
    End Property

#End Region

    Public Sub New()
        _nom = ""
        _prenom = ""
        _adresse = ""
        _ville = ""
        _telephone = ""
        _courriel = ""
    End Sub

    Public Sub New(nom As String, prenom As String, adresse As String, ville As String, telephone As String, courriel As String)
        _nom = nom
        _prenom = prenom
        _adresse = adresse
        _ville = ville
        _telephone = telephone
        _courriel = courriel
    End Sub

End Class
