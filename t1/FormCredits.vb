﻿Public Class FormCredits

    Private licence As String = "CC-By-SA 4.0"
    Private licenceURL As String = "https://creativecommons.org/licenses/by-sa/4.0/deed.fr"
    Private sourceURL As String = "https://bitbucket.org/vergerk/jeudessais"

    Private siteURL As String = "http://www.vergerk.fr/"

    Private Sub FormCredits_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim myCallback As New Image.GetThumbnailImageAbort(AddressOf ThumbnailCallback)
        pb_icone.Image = Image.FromFile("Ressources\icone.ico").GetThumbnailImage(pb_icone.Size.Width, pb_icone.Size.Height, myCallback, System.IntPtr.Zero)

        lb_application.Text = Application.ProductName

        rtb_credits.Text =
            "VERGER Khevin - 2017 - " + siteURL + vbCr +
            "" + vbCr +
            "Licence Creative Commons " + licence + vbCr +
            licenceURL + vbCr +
            "" + vbCr +
            "Sources disponibles à l'adresse : " + sourceURL + vbCr +
            "" + vbCr +
            "Toute modification ou/et exploitation sous une autre licence ou contraire à la licence actuelle n'est pas autorisée." + vbCr +
            "" + vbCr +
            "Connecteur ADO.Net MySQL (Oracle) sous licence GPL."
    End Sub

    ''' <summary>
    ''' Ferme le formulaire
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btn_fermer_Click(sender As Object, e As EventArgs) Handles btn_fermer.Click
        Close()
    End Sub

    ''' <summary>
    ''' Ouvre la page web correspondant au lien cliqué
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub rtb_credits_LinkClicked(sender As Object, e As LinkClickedEventArgs) Handles rtb_credits.LinkClicked
        System.Diagnostics.Process.Start(e.LinkText)
    End Sub

    Public Function ThumbnailCallback() As Boolean
        Return False
    End Function

End Class