﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormPrincipale
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormPrincipale))
        Me.btn_prenoms = New System.Windows.Forms.Button()
        Me.OpenFileDialog = New System.Windows.Forms.OpenFileDialog()
        Me.dgv_liste = New System.Windows.Forms.DataGridView()
        Me.nom_dgc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.prenom_dgc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.adresse_dgc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ville_dgc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tel_dgc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.courriel_dgc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Btn_genDgv = New System.Windows.Forms.Button()
        Me.ProgressBar = New System.Windows.Forms.ProgressBar()
        Me.btn_export = New System.Windows.Forms.Button()
        Me.SaveFileDialog = New System.Windows.Forms.SaveFileDialog()
        Me.txt_prenoms = New System.Windows.Forms.TextBox()
        Me.txt_noms = New System.Windows.Forms.TextBox()
        Me.btn_noms = New System.Windows.Forms.Button()
        Me.txt_adresses = New System.Windows.Forms.TextBox()
        Me.btn_adresses = New System.Windows.Forms.Button()
        Me.txt_villes = New System.Windows.Forms.TextBox()
        Me.btn_villes = New System.Windows.Forms.Button()
        Me.txt_colPrenoms = New System.Windows.Forms.TextBox()
        Me.txt_colNoms = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_colAdresses = New System.Windows.Forms.TextBox()
        Me.txt_colVilles = New System.Windows.Forms.TextBox()
        Me.ckb_prenoms = New System.Windows.Forms.CheckBox()
        Me.ckb_noms = New System.Windows.Forms.CheckBox()
        Me.ckb_adresses = New System.Windows.Forms.CheckBox()
        Me.ckb_villes = New System.Windows.Forms.CheckBox()
        Me.txt_sepVilles = New System.Windows.Forms.TextBox()
        Me.txt_sepAdresses = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txt_sepNoms = New System.Windows.Forms.TextBox()
        Me.txt_sepPrenoms = New System.Windows.Forms.TextBox()
        Me.btn_extrVilles = New System.Windows.Forms.Button()
        Me.btn_extrAdresses = New System.Windows.Forms.Button()
        Me.btn_extrNoms = New System.Windows.Forms.Button()
        Me.btn_extrPrenoms = New System.Windows.Forms.Button()
        Me.lb_cptePrenoms = New System.Windows.Forms.Label()
        Me.lb_cpteNoms = New System.Windows.Forms.Label()
        Me.lb_cpteAdresses = New System.Windows.Forms.Label()
        Me.lb_cpteVilles = New System.Windows.Forms.Label()
        Me.btn_aProposDe = New System.Windows.Forms.Button()
        Me.cb_listeExports = New System.Windows.Forms.ComboBox()
        CType(Me.dgv_liste, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btn_prenoms
        '
        Me.btn_prenoms.Location = New System.Drawing.Point(12, 35)
        Me.btn_prenoms.Name = "btn_prenoms"
        Me.btn_prenoms.Size = New System.Drawing.Size(74, 23)
        Me.btn_prenoms.TabIndex = 1
        Me.btn_prenoms.Text = "Prénoms"
        Me.btn_prenoms.UseVisualStyleBackColor = True
        '
        'dgv_liste
        '
        Me.dgv_liste.AllowUserToAddRows = False
        Me.dgv_liste.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgv_liste.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgv_liste.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv_liste.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.nom_dgc, Me.prenom_dgc, Me.adresse_dgc, Me.ville_dgc, Me.tel_dgc, Me.courriel_dgc})
        Me.dgv_liste.Location = New System.Drawing.Point(12, 243)
        Me.dgv_liste.Name = "dgv_liste"
        Me.dgv_liste.RowHeadersVisible = False
        Me.dgv_liste.Size = New System.Drawing.Size(694, 153)
        Me.dgv_liste.TabIndex = 2
        Me.dgv_liste.TabStop = False
        '
        'nom_dgc
        '
        Me.nom_dgc.HeaderText = "Nom"
        Me.nom_dgc.Name = "nom_dgc"
        '
        'prenom_dgc
        '
        Me.prenom_dgc.HeaderText = "Prénom"
        Me.prenom_dgc.Name = "prenom_dgc"
        '
        'adresse_dgc
        '
        Me.adresse_dgc.HeaderText = "Adresse"
        Me.adresse_dgc.Name = "adresse_dgc"
        '
        'ville_dgc
        '
        Me.ville_dgc.HeaderText = "Ville"
        Me.ville_dgc.Name = "ville_dgc"
        '
        'tel_dgc
        '
        Me.tel_dgc.HeaderText = "Téléphone"
        Me.tel_dgc.Name = "tel_dgc"
        '
        'courriel_dgc
        '
        Me.courriel_dgc.HeaderText = "Courriel"
        Me.courriel_dgc.Name = "courriel_dgc"
        '
        'Btn_genDgv
        '
        Me.Btn_genDgv.Location = New System.Drawing.Point(12, 214)
        Me.Btn_genDgv.Name = "Btn_genDgv"
        Me.Btn_genDgv.Size = New System.Drawing.Size(161, 23)
        Me.Btn_genDgv.TabIndex = 21
        Me.Btn_genDgv.Text = "Remplir le tableau"
        Me.Btn_genDgv.UseVisualStyleBackColor = True
        '
        'ProgressBar
        '
        Me.ProgressBar.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ProgressBar.Location = New System.Drawing.Point(12, 402)
        Me.ProgressBar.Name = "ProgressBar"
        Me.ProgressBar.Size = New System.Drawing.Size(694, 10)
        Me.ProgressBar.TabIndex = 4
        '
        'btn_export
        '
        Me.btn_export.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_export.Enabled = False
        Me.btn_export.Location = New System.Drawing.Point(612, 214)
        Me.btn_export.Name = "btn_export"
        Me.btn_export.Size = New System.Drawing.Size(94, 23)
        Me.btn_export.TabIndex = 22
        Me.btn_export.Text = "Exporter ..."
        Me.btn_export.UseVisualStyleBackColor = True
        '
        'SaveFileDialog
        '
        Me.SaveFileDialog.Filter = "XML|*.xml|CSV|*.csv"
        '
        'txt_prenoms
        '
        Me.txt_prenoms.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_prenoms.Location = New System.Drawing.Point(142, 37)
        Me.txt_prenoms.Name = "txt_prenoms"
        Me.txt_prenoms.Size = New System.Drawing.Size(201, 20)
        Me.txt_prenoms.TabIndex = 6
        Me.txt_prenoms.TabStop = False
        '
        'txt_noms
        '
        Me.txt_noms.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_noms.Location = New System.Drawing.Point(142, 66)
        Me.txt_noms.Name = "txt_noms"
        Me.txt_noms.Size = New System.Drawing.Size(201, 20)
        Me.txt_noms.TabIndex = 8
        Me.txt_noms.TabStop = False
        '
        'btn_noms
        '
        Me.btn_noms.Location = New System.Drawing.Point(12, 64)
        Me.btn_noms.Name = "btn_noms"
        Me.btn_noms.Size = New System.Drawing.Size(74, 23)
        Me.btn_noms.TabIndex = 6
        Me.btn_noms.Text = "Noms"
        Me.btn_noms.UseVisualStyleBackColor = True
        '
        'txt_adresses
        '
        Me.txt_adresses.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_adresses.Location = New System.Drawing.Point(142, 95)
        Me.txt_adresses.Name = "txt_adresses"
        Me.txt_adresses.Size = New System.Drawing.Size(201, 20)
        Me.txt_adresses.TabIndex = 10
        Me.txt_adresses.TabStop = False
        '
        'btn_adresses
        '
        Me.btn_adresses.Location = New System.Drawing.Point(12, 93)
        Me.btn_adresses.Name = "btn_adresses"
        Me.btn_adresses.Size = New System.Drawing.Size(74, 23)
        Me.btn_adresses.TabIndex = 11
        Me.btn_adresses.Text = "Adresses"
        Me.btn_adresses.UseVisualStyleBackColor = True
        '
        'txt_villes
        '
        Me.txt_villes.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_villes.Location = New System.Drawing.Point(142, 124)
        Me.txt_villes.Name = "txt_villes"
        Me.txt_villes.Size = New System.Drawing.Size(201, 20)
        Me.txt_villes.TabIndex = 12
        Me.txt_villes.TabStop = False
        '
        'btn_villes
        '
        Me.btn_villes.Location = New System.Drawing.Point(12, 122)
        Me.btn_villes.Name = "btn_villes"
        Me.btn_villes.Size = New System.Drawing.Size(74, 23)
        Me.btn_villes.TabIndex = 16
        Me.btn_villes.Text = "Villes"
        Me.btn_villes.UseVisualStyleBackColor = True
        '
        'txt_colPrenoms
        '
        Me.txt_colPrenoms.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_colPrenoms.Location = New System.Drawing.Point(349, 37)
        Me.txt_colPrenoms.Name = "txt_colPrenoms"
        Me.txt_colPrenoms.Size = New System.Drawing.Size(58, 20)
        Me.txt_colPrenoms.TabIndex = 3
        Me.txt_colPrenoms.Text = "0"
        Me.txt_colPrenoms.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_colNoms
        '
        Me.txt_colNoms.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_colNoms.Location = New System.Drawing.Point(349, 66)
        Me.txt_colNoms.Name = "txt_colNoms"
        Me.txt_colNoms.Size = New System.Drawing.Size(58, 20)
        Me.txt_colNoms.TabIndex = 8
        Me.txt_colNoms.Text = "0"
        Me.txt_colNoms.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.Location = New System.Drawing.Point(349, 2)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(58, 32)
        Me.Label2.TabIndex = 18
        Me.Label2.Text = "Colonne (début = 0)"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txt_colAdresses
        '
        Me.txt_colAdresses.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_colAdresses.Location = New System.Drawing.Point(349, 95)
        Me.txt_colAdresses.Name = "txt_colAdresses"
        Me.txt_colAdresses.Size = New System.Drawing.Size(58, 20)
        Me.txt_colAdresses.TabIndex = 13
        Me.txt_colAdresses.Text = "0"
        Me.txt_colAdresses.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_colVilles
        '
        Me.txt_colVilles.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_colVilles.Location = New System.Drawing.Point(349, 124)
        Me.txt_colVilles.Name = "txt_colVilles"
        Me.txt_colVilles.Size = New System.Drawing.Size(58, 20)
        Me.txt_colVilles.TabIndex = 18
        Me.txt_colVilles.Text = "0"
        Me.txt_colVilles.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'ckb_prenoms
        '
        Me.ckb_prenoms.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ckb_prenoms.AutoSize = True
        Me.ckb_prenoms.Location = New System.Drawing.Point(466, 39)
        Me.ckb_prenoms.Name = "ckb_prenoms"
        Me.ckb_prenoms.Size = New System.Drawing.Size(151, 17)
        Me.ckb_prenoms.TabIndex = 5
        Me.ckb_prenoms.Text = "Commencer à la 1ère ligne"
        Me.ckb_prenoms.UseVisualStyleBackColor = True
        '
        'ckb_noms
        '
        Me.ckb_noms.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ckb_noms.AutoSize = True
        Me.ckb_noms.Location = New System.Drawing.Point(466, 68)
        Me.ckb_noms.Name = "ckb_noms"
        Me.ckb_noms.Size = New System.Drawing.Size(151, 17)
        Me.ckb_noms.TabIndex = 10
        Me.ckb_noms.Text = "Commencer à la 1ère ligne"
        Me.ckb_noms.UseVisualStyleBackColor = True
        '
        'ckb_adresses
        '
        Me.ckb_adresses.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ckb_adresses.AutoSize = True
        Me.ckb_adresses.Location = New System.Drawing.Point(466, 97)
        Me.ckb_adresses.Name = "ckb_adresses"
        Me.ckb_adresses.Size = New System.Drawing.Size(151, 17)
        Me.ckb_adresses.TabIndex = 15
        Me.ckb_adresses.Text = "Commencer à la 1ère ligne"
        Me.ckb_adresses.UseVisualStyleBackColor = True
        '
        'ckb_villes
        '
        Me.ckb_villes.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ckb_villes.AutoSize = True
        Me.ckb_villes.Location = New System.Drawing.Point(466, 126)
        Me.ckb_villes.Name = "ckb_villes"
        Me.ckb_villes.Size = New System.Drawing.Size(151, 17)
        Me.ckb_villes.TabIndex = 20
        Me.ckb_villes.Text = "Commencer à la 1ère ligne"
        Me.ckb_villes.UseVisualStyleBackColor = True
        '
        'txt_sepVilles
        '
        Me.txt_sepVilles.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_sepVilles.Location = New System.Drawing.Point(413, 124)
        Me.txt_sepVilles.MaxLength = 1
        Me.txt_sepVilles.Name = "txt_sepVilles"
        Me.txt_sepVilles.Size = New System.Drawing.Size(47, 20)
        Me.txt_sepVilles.TabIndex = 19
        Me.txt_sepVilles.Text = ";"
        Me.txt_sepVilles.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_sepAdresses
        '
        Me.txt_sepAdresses.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_sepAdresses.Location = New System.Drawing.Point(413, 95)
        Me.txt_sepAdresses.MaxLength = 1
        Me.txt_sepAdresses.Name = "txt_sepAdresses"
        Me.txt_sepAdresses.Size = New System.Drawing.Size(47, 20)
        Me.txt_sepAdresses.TabIndex = 14
        Me.txt_sepAdresses.Text = ";"
        Me.txt_sepAdresses.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.Location = New System.Drawing.Point(413, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(47, 13)
        Me.Label1.TabIndex = 29
        Me.Label1.Text = "Séparat."
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txt_sepNoms
        '
        Me.txt_sepNoms.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_sepNoms.Location = New System.Drawing.Point(413, 66)
        Me.txt_sepNoms.MaxLength = 1
        Me.txt_sepNoms.Name = "txt_sepNoms"
        Me.txt_sepNoms.Size = New System.Drawing.Size(47, 20)
        Me.txt_sepNoms.TabIndex = 9
        Me.txt_sepNoms.Text = ";"
        Me.txt_sepNoms.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_sepPrenoms
        '
        Me.txt_sepPrenoms.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_sepPrenoms.Location = New System.Drawing.Point(413, 37)
        Me.txt_sepPrenoms.MaxLength = 1
        Me.txt_sepPrenoms.Name = "txt_sepPrenoms"
        Me.txt_sepPrenoms.Size = New System.Drawing.Size(47, 20)
        Me.txt_sepPrenoms.TabIndex = 4
        Me.txt_sepPrenoms.Text = ";"
        Me.txt_sepPrenoms.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btn_extrVilles
        '
        Me.btn_extrVilles.Location = New System.Drawing.Point(92, 122)
        Me.btn_extrVilles.Name = "btn_extrVilles"
        Me.btn_extrVilles.Size = New System.Drawing.Size(44, 23)
        Me.btn_extrVilles.TabIndex = 17
        Me.btn_extrVilles.Text = "Extr."
        Me.btn_extrVilles.UseVisualStyleBackColor = True
        '
        'btn_extrAdresses
        '
        Me.btn_extrAdresses.Location = New System.Drawing.Point(92, 93)
        Me.btn_extrAdresses.Name = "btn_extrAdresses"
        Me.btn_extrAdresses.Size = New System.Drawing.Size(44, 23)
        Me.btn_extrAdresses.TabIndex = 12
        Me.btn_extrAdresses.Text = "Extr."
        Me.btn_extrAdresses.UseVisualStyleBackColor = True
        '
        'btn_extrNoms
        '
        Me.btn_extrNoms.Location = New System.Drawing.Point(92, 64)
        Me.btn_extrNoms.Name = "btn_extrNoms"
        Me.btn_extrNoms.Size = New System.Drawing.Size(44, 23)
        Me.btn_extrNoms.TabIndex = 7
        Me.btn_extrNoms.Text = "Extr."
        Me.btn_extrNoms.UseVisualStyleBackColor = True
        '
        'btn_extrPrenoms
        '
        Me.btn_extrPrenoms.Location = New System.Drawing.Point(92, 35)
        Me.btn_extrPrenoms.Name = "btn_extrPrenoms"
        Me.btn_extrPrenoms.Size = New System.Drawing.Size(44, 23)
        Me.btn_extrPrenoms.TabIndex = 2
        Me.btn_extrPrenoms.Text = "Extr."
        Me.btn_extrPrenoms.UseVisualStyleBackColor = True
        '
        'lb_cptePrenoms
        '
        Me.lb_cptePrenoms.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lb_cptePrenoms.Location = New System.Drawing.Point(643, 35)
        Me.lb_cptePrenoms.Name = "lb_cptePrenoms"
        Me.lb_cptePrenoms.Size = New System.Drawing.Size(63, 23)
        Me.lb_cptePrenoms.TabIndex = 36
        Me.lb_cptePrenoms.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lb_cpteNoms
        '
        Me.lb_cpteNoms.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lb_cpteNoms.Location = New System.Drawing.Point(643, 69)
        Me.lb_cpteNoms.Name = "lb_cpteNoms"
        Me.lb_cpteNoms.Size = New System.Drawing.Size(63, 13)
        Me.lb_cpteNoms.TabIndex = 37
        Me.lb_cpteNoms.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lb_cpteAdresses
        '
        Me.lb_cpteAdresses.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lb_cpteAdresses.Location = New System.Drawing.Point(643, 98)
        Me.lb_cpteAdresses.Name = "lb_cpteAdresses"
        Me.lb_cpteAdresses.Size = New System.Drawing.Size(63, 13)
        Me.lb_cpteAdresses.TabIndex = 38
        Me.lb_cpteAdresses.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lb_cpteVilles
        '
        Me.lb_cpteVilles.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lb_cpteVilles.Location = New System.Drawing.Point(643, 127)
        Me.lb_cpteVilles.Name = "lb_cpteVilles"
        Me.lb_cpteVilles.Size = New System.Drawing.Size(63, 13)
        Me.lb_cpteVilles.TabIndex = 39
        Me.lb_cpteVilles.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btn_aProposDe
        '
        Me.btn_aProposDe.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_aProposDe.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_aProposDe.Location = New System.Drawing.Point(612, 7)
        Me.btn_aProposDe.Name = "btn_aProposDe"
        Me.btn_aProposDe.Size = New System.Drawing.Size(94, 23)
        Me.btn_aProposDe.TabIndex = 23
        Me.btn_aProposDe.Text = "A propos de ..."
        Me.btn_aProposDe.UseVisualStyleBackColor = True
        '
        'cb_listeExports
        '
        Me.cb_listeExports.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cb_listeExports.FormattingEnabled = True
        Me.cb_listeExports.Location = New System.Drawing.Point(496, 216)
        Me.cb_listeExports.Name = "cb_listeExports"
        Me.cb_listeExports.Size = New System.Drawing.Size(110, 21)
        Me.cb_listeExports.TabIndex = 40
        '
        'FormPrincipale
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(718, 424)
        Me.Controls.Add(Me.cb_listeExports)
        Me.Controls.Add(Me.btn_aProposDe)
        Me.Controls.Add(Me.lb_cpteVilles)
        Me.Controls.Add(Me.lb_cpteAdresses)
        Me.Controls.Add(Me.lb_cpteNoms)
        Me.Controls.Add(Me.lb_cptePrenoms)
        Me.Controls.Add(Me.btn_extrVilles)
        Me.Controls.Add(Me.btn_extrAdresses)
        Me.Controls.Add(Me.btn_extrNoms)
        Me.Controls.Add(Me.btn_extrPrenoms)
        Me.Controls.Add(Me.txt_sepVilles)
        Me.Controls.Add(Me.txt_sepAdresses)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txt_sepNoms)
        Me.Controls.Add(Me.txt_sepPrenoms)
        Me.Controls.Add(Me.ckb_villes)
        Me.Controls.Add(Me.ckb_adresses)
        Me.Controls.Add(Me.ckb_noms)
        Me.Controls.Add(Me.ckb_prenoms)
        Me.Controls.Add(Me.txt_colVilles)
        Me.Controls.Add(Me.txt_colAdresses)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txt_colNoms)
        Me.Controls.Add(Me.txt_colPrenoms)
        Me.Controls.Add(Me.txt_villes)
        Me.Controls.Add(Me.btn_villes)
        Me.Controls.Add(Me.txt_adresses)
        Me.Controls.Add(Me.btn_adresses)
        Me.Controls.Add(Me.txt_noms)
        Me.Controls.Add(Me.btn_noms)
        Me.Controls.Add(Me.txt_prenoms)
        Me.Controls.Add(Me.btn_export)
        Me.Controls.Add(Me.ProgressBar)
        Me.Controls.Add(Me.Btn_genDgv)
        Me.Controls.Add(Me.dgv_liste)
        Me.Controls.Add(Me.btn_prenoms)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(600, 450)
        Me.Name = "FormPrincipale"
        Me.Text = "Générateur de jeu d'essais"
        CType(Me.dgv_liste, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btn_prenoms As Button
    Friend WithEvents OpenFileDialog As OpenFileDialog
    Friend WithEvents dgv_liste As DataGridView
    Friend WithEvents Btn_genDgv As Button
    Friend WithEvents ProgressBar As System.Windows.Forms.ProgressBar
    Friend WithEvents btn_export As System.Windows.Forms.Button
    Friend WithEvents SaveFileDialog As System.Windows.Forms.SaveFileDialog
    Friend WithEvents txt_prenoms As System.Windows.Forms.TextBox
    Friend WithEvents txt_noms As System.Windows.Forms.TextBox
    Friend WithEvents btn_noms As System.Windows.Forms.Button
    Friend WithEvents txt_adresses As System.Windows.Forms.TextBox
    Friend WithEvents btn_adresses As System.Windows.Forms.Button
    Friend WithEvents txt_villes As System.Windows.Forms.TextBox
    Friend WithEvents btn_villes As System.Windows.Forms.Button
    Friend WithEvents txt_colPrenoms As System.Windows.Forms.TextBox
    Friend WithEvents txt_colNoms As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txt_colAdresses As System.Windows.Forms.TextBox
    Friend WithEvents txt_colVilles As System.Windows.Forms.TextBox
    Friend WithEvents ckb_prenoms As System.Windows.Forms.CheckBox
    Friend WithEvents ckb_noms As System.Windows.Forms.CheckBox
    Friend WithEvents ckb_adresses As System.Windows.Forms.CheckBox
    Friend WithEvents ckb_villes As System.Windows.Forms.CheckBox
    Friend WithEvents txt_sepVilles As System.Windows.Forms.TextBox
    Friend WithEvents txt_sepAdresses As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txt_sepNoms As System.Windows.Forms.TextBox
    Friend WithEvents txt_sepPrenoms As System.Windows.Forms.TextBox
    Friend WithEvents btn_extrVilles As System.Windows.Forms.Button
    Friend WithEvents btn_extrAdresses As System.Windows.Forms.Button
    Friend WithEvents btn_extrNoms As System.Windows.Forms.Button
    Friend WithEvents btn_extrPrenoms As System.Windows.Forms.Button
    Friend WithEvents lb_cptePrenoms As System.Windows.Forms.Label
    Friend WithEvents lb_cpteNoms As System.Windows.Forms.Label
    Friend WithEvents lb_cpteAdresses As System.Windows.Forms.Label
    Friend WithEvents lb_cpteVilles As System.Windows.Forms.Label
    Friend WithEvents nom_dgc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents prenom_dgc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents adresse_dgc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ville_dgc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tel_dgc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents courriel_dgc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btn_aProposDe As System.Windows.Forms.Button
    Friend WithEvents cb_listeExports As System.Windows.Forms.ComboBox
End Class
