Ce logiciel extrait les informations de fichiers CSV (prénoms, noms, adressses, code postal et ville) et génère des adresses courriels ainsi que des numéros de téléphone.
Le but est la création d'un jeu d'essais complet, pouvant être exploité à des fins de tests dans d'autres projets.

Chaque jeu est exportable au format XML, CSV ainsi que dans une base de données MySQL.

Licence Creative Commons CC-By-SA 4.0
https://creativecommons.org/licenses/by-sa/4.0/deed.fr

Connecteur ADO.Net MySQL (Oracle) sous licence GPL